{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **[Project] DNA Sequence Prediction for Compression**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use data from Illumina's [Platinum Genomes](https://emea.illumina.com/platinumgenomes.html) project.\n",
    "\n",
    "In this project, they performed whole-genome sequencing (WGS) of the 17 member [CEPH pedigree 1463](https://www.coriell.org/0/Sections/Collections/NIGMS/CEPHFamiliesDetail.aspx?PgId=441&fam=1463&coll=GM) (17 human individuals, 3 generations) on Illumina HiSeq systems to provide a set of high-accuracy human WGS data.\n",
    "\n",
    "> See the [Platinum Genomes manuscript](http://dx.doi.org/10.1101/gr.210500.116) for a full description of the project.\n",
    "\n",
    "In particular, Illumina has sequenced the individuals NA12877 and NA12878 to 200x depth on a HiSeq 2000 system.\n",
    "These data are available via the European Nucleotide Archive (ENA) under accession code [PRJEB3246](https://www.ebi.ac.uk/ena/browser/view/PRJEB3246).\n",
    "\n",
    "> The PRJEB3246 data is also part of the the [MPEG-G Genomic Information Database](https://mpeg.chiariglione.org/standards/mpeg-g/genomic-information-representation/mpeg-g-genomic-information-database-4) (ID 01).\n",
    "\n",
    "We will only use a subset of the PRJEB3246 data.\n",
    "We extracted the first 10,000,000 records (i.e., the first 40,000,000 lines) from two FASTQ files (`ERR174310_1.fastq` and `ERR174310_2.fastq`) generated from NA12877."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data access"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The download from the TNT homepage is straightforward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! wget http://www.tnt.uni-hannover.de/edu/vorlesungen/AMLG/data/project-dna-sequence-prediction.tar.gz\n",
    "! tar -xzvf project-dna-sequence-prediction.tar.gz\n",
    "! mv -v project-dna-sequence-prediction/ data/\n",
    "! rm -v project-dna-sequence-prediction.tar.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the `data/` folder you will now find two files: `reads_1.fastq.gz` and `reads_2.fastq.gz`.\n",
    "First, you need to decompress the files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! gunzip data/reads_1.fastq.gz\n",
    "! gunzip data/reads_2.fastq.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, you can verify they contain the correct number of FASTQ records:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! wc -l data/reads_1.fastq\n",
    "! wc -l data/reads_2.fastq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [FASTQ format](https://en.wikipedia.org/wiki/FASTQ_format) is the de-facto standard for storing both a biological sequence (usually nucleotide sequence) and its corresponding quality scores.\n",
    "Both the sequence letter and quality score are each encoded with a single ASCII character.\n",
    "\n",
    "Each sequence, i.e., read, is represented by a single FASTQ record, which consists of four lines:\n",
    "- The first line contains the **read identifier**. It starts with `@`. Typically, sequencing machine vendors generate read identifiers in a proprietary systematic way.\n",
    "- The second line contains the **sequence**, where each symbol is represented with a single ASCII character.\n",
    "- The third line starts with `+` and contains an optional **description**. Usually this line is left empty; it then only contains `+` as separator between the sequence and the quality scores.\n",
    "- The fourth line contains the **quality scores**. A quality score is a value indicating the confidence in a base call.\n",
    "\n",
    "The following function can be used to parse a FASTQ file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Dict, List\n",
    "\n",
    "\n",
    "def parse_fastq_file(file_path: str, n_records: int = None) -> List[Dict[str, str]]:\n",
    "    \"\"\"Parse a FASTQ file.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    file_path : str\n",
    "        The path to the FASTQ file.\n",
    "    n_records : int\n",
    "        The number of FASTQ records to parse. The default value is 'None'; in\n",
    "        this case, the entire FASTQ file will be parsed.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    records : list[dict[str, str]]\n",
    "        A list of dictionaries, where each dictionary contains one FASTQ\n",
    "        record.\n",
    "\n",
    "    \"\"\"\n",
    "\n",
    "    with open(file=file_path, mode=\"r\") as file:\n",
    "        records = []\n",
    "        lines = []\n",
    "        for line in file:\n",
    "            lines.append(line.rstrip())\n",
    "            if (len(lines)) == 4:\n",
    "                if n_records == None or len(records) < n_records:\n",
    "                    records.append(dict(zip([\"id\", \"seq\", \"desc\", \"qual\"], lines)))\n",
    "                    lines = []\n",
    "                else:\n",
    "                    break\n",
    "\n",
    "        return records"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "records = parse_fastq_file(file_path=\"data/reads_1.fastq\", n_records=20)\n",
    "\n",
    "for i, record in enumerate(records):\n",
    "    print(f\"Record {i:2}: {record}\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "amlg",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
